/*
 * Copyright (C) 2015 Por Geeks
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.porgeeks.cia;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import com.porgeeks.cia.graficos.Login;
import com.porgeeks.cia.graficos.Empresa;
import com.porgeeks.cia.graficos.Servidor;

/**
 *
 * @author Luis Yair Miranda G.
 */
public class Run {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        try {
            JFrame.setDefaultLookAndFeelDecorated(true);
            JDialog.setDefaultLookAndFeelDecorated(true);
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e){}
        if (args.length > 0){
            // Si ingresa un modo
            for (String key: args)
            {
                if(key.toUpperCase().equals("-empresa")){
                    // Configura los datos de la empresa
                    java.awt.EventQueue.invokeLater(() -> {
                        new Empresa().setVisible(true);
                        }
                    );
                }
                if(key.toUpperCase().equals("-asistente")){
                    // Inicia el asistente de configuracion inicial
                }
                if(key.toUpperCase().equals("-servidor")){
                    // Configura la conexion del servidor por primera vez
                    java.awt.EventQueue.invokeLater(() -> {
                        new Servidor().setVisible(true);
                        }
                    );
                }
                if(key.toUpperCase().equals("-H")){
                    // Imprimo la ayuda y salgo
                    System.out.println("Modo de Uso:");
                    System.out.println("\t -servidor   Inicia el Server en modo gráfico (gui) o en modo consola (text)");
                    System.err.println("\t -empresa    Permite ingresar los datos de la empresa");
                    System.out.println("\t -asistente  Ejecute el asistente de configuracion inicial");
                    System.out.println("\t -h          Muestra este mensaje de ayuda");
                    System.exit(0);
                }
 
            }
        }else{
            java.awt.EventQueue.invokeLater(() -> {
//                if(){
//                    
//                }
                new Login().setVisible(true);
                }
            );
        }
    }
}
