/*
 * Copyright (C) 2015 YAIE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.porgeeks.cia.sql;

import com.porgeeks.cia.herramientas.Funciones;
import java.util.Properties;
import javax.sql.DataSource;
import org.apache.commons.dbcp2.BasicDataSource;


/**
 *
 * @author YAIE
 */
public class ConectarBD {
    public DataSource dataSource;
    Funciones f = new Funciones();
    Properties p = new Properties(f.Cargar("Servidor"));
    private final String driver = p.getProperty("driverClassName");
    private final String db = p.getProperty("base");
    private final String url = p.getProperty("jdbc")+p.getProperty("ruta")+":"+p.getProperty("puerto")+"/"+db;
    private final String user = p.getProperty("username");
    private final String pass = p.getProperty("password");
    private final String validar=p.getProperty("validar");

    /**
     *
     */
    public ConectarBD(){
        inicializaDataSource();
    }

    private void inicializaDataSource(){
        BasicDataSource basicDataSource = new BasicDataSource();
        basicDataSource.setDriverClassName(driver);
        basicDataSource.setUsername(user);
        basicDataSource.setPassword(pass);
        basicDataSource.setUrl(url);
        basicDataSource.setInitialSize(5);
        basicDataSource.setMaxTotal(90);
        basicDataSource.setMinIdle(10);
        basicDataSource.setValidationQuery(validar);
        dataSource = basicDataSource;
    }
}