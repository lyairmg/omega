/*
 * Copyright (C) 2015 Por Geeks
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.porgeeks.cia.sql;

import com.porgeeks.cia.herramientas.Funciones;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;


/**
 * Clase con todas las funciones para Agregar, Editar, Consultar y Eliminar informacion
 * de la base de datos.
 *
 * @author Luis Yair Miranda G.
 */
public class FuncionesSQL {
    Funciones f = new Funciones();
    ConectarBD metodospool = new ConectarBD();
    Connection conect = null;
    Statement st;
    String SSQL;
    ArrayList da;
    
    /**
     * Funcion para la validacion de un usuario existente con su respectiva contraseña 
     * 
     * @param usuario Usuario agregado con permiso de empleado para operar el sistema
     * @param clave se encuentra codificada en SHA1 medinate librerias de APache
     * @return Retorna un 0 si es falso y 1 si es verdadero
     */
    public int Login(String usuario, char[] clave) {
        //Si el resultado es 1 el login fue validado con exito de lo contrario se rechaza.
        int resultado=0;
        String pass = f.EncriptarSHA1(String.valueOf(clave));
        SSQL="SELECT * FROM empleados WHERE idEmple='"+usuario+"' AND password='"+pass+"'";
        
        try {
            conect = metodospool.dataSource.getConnection();
            st = conect.createStatement();
            ResultSet rs = st.executeQuery(SSQL);
            if(rs.next()){
                resultado=1;
            }
            conect.close();
        } catch (SQLException ex) {
            System.err.println("Error de conexión"+ex);
        }finally{
            try {
                conect.close();
            } catch (SQLException ex) {
                System.err.println("Error de desconexion"+ex);
            }
        }
    return resultado;
    }
    
    /**
     * Validador perosnalizado con unresultado boleano(int)
     * 
     * @param consulta String que posee una consulta SQL completa
     * @return Retorna unvalor numerico (int) 1 si es verdadero y 0 para falso
     */
    public int ValidarPersonalizado(String consulta){
        int resultado=0;
        this.SSQL=consulta;
        try {
            conect = metodospool.dataSource.getConnection();
            st = conect.createStatement();
            ResultSet rs = st.executeQuery(SSQL);
            if(rs.next()){
                resultado=1;
            }
            conect.close();
        } catch (SQLException ex) {
            System.err.println("Error de conexión"+ex);
        }finally{
            try {
                conect.close();
            } catch (SQLException ex) {
                System.err.println("Error de desconexion"+ex);
            }
        }
    return resultado;
    }
    
    /**
     * Realiza un rellenado de un JTable
     * 
     * @param tabla Componete Swing donde es cargada la tabla de MySQL
     * @param nombre Nombre de la tabla a consultar de MySQL
     * @param campos Campos de consulta de la tabla
     * @param columna Nombre de las columnas que tendra el componente tabla de Java Swing
     */
    public void RellenarTabla(JTable tabla, String nombre, String campos, String[] columna){
	try{
            int columnas = columna.length;
            SSQL=("SELECT "+campos+" FROM "+nombre+";");
            conect = metodospool.dataSource.getConnection();
            st = conect.createStatement();
            try (ResultSet rs = st.executeQuery(SSQL)) {
                Object datos[]=new Object[columnas];
                DefaultTableModel modelotabla;
                modelotabla = new DefaultTableModel(null, columna);
                tabla.setModel(modelotabla);
                while(rs.next()){
                    for(int i=0;i<columnas;i++){
                        datos[i]=rs.getObject(i+1);
                    }
                    modelotabla.addRow(datos);
                }
                conect.close();
            }
	}catch(SQLException ex){
            JOptionPane.showMessageDialog(null, "Error: "+ex.getMessage());
        }
    }
    
    /**
     * Consulta y filtra por medio de un idConsulta una o mas filas de informacion de una tabla MySQL
     * 
     * @param tabla Componete JTable donde se almacenara la busqueda
     * @param nombre Nombre de la tabla MySQL
     * @param campos Campos a mostrar dentro del componete JTable
     * @param columna Nombre de las columnas que recibirá el JTable
     * @param idConsulta Valor a buscar en la tabla
     * @param id Nombre de la columna que almacena el idConsulta
     * @param a Numero de relleno para la consulta por defecto 0
     */
    public void RellenarTablaBusqueda(JTable tabla, String nombre, String campos, String[] columna, String idConsulta, String id, int a){
	try{
            int columnas = a+columna.length;
            SSQL=("SELECT "+campos+" FROM "+nombre+" WHERE "+id+"='"+idConsulta+"';");
            conect = metodospool.dataSource.getConnection();
            st = conect.createStatement();
            try (ResultSet rs = st.executeQuery(SSQL)) {
                Object datos[]=new Object[columnas];
                DefaultTableModel modelotabla;
                modelotabla = new DefaultTableModel(null, columna);
                tabla.setModel(modelotabla);
                while(rs.next()){
                    for(int i=0;i<columnas;i++){
                        datos[i]=rs.getObject(i+1);
                    }
                    modelotabla.addRow(datos);
                }
                conect.close();
            }
	}catch(SQLException ex){
            JOptionPane.showMessageDialog(null, "Error: "+ex.getMessage());
        }
    }
    
    /**
     *
     * @param nombre Nombre de la tabla MySQL
     * @param campos Campos a seleccionar en la consulta de MySQL
     * @param columna Columnas a mostrar en JTable
     * @param idConsulta Valor que filtra la busqueda
     * @param id Nombre que recibe la columna en la tabla MySQL
     * @param a Valor de relleno, por defecto es 0;
     * @return Retorna un arraylist de la consulta realizada.
     */
    public ArrayList AgregarTablaBusqueda(String nombre, String campos, String[] columna, String idConsulta, String id, int a){
	try{
            int columnas = a+columna.length;
            SSQL=("SELECT "+campos+" FROM "+nombre+" WHERE "+id+"='"+idConsulta+"';");
            conect = metodospool.dataSource.getConnection();
            st = conect.createStatement();
            try (ResultSet rs = st.executeQuery(SSQL)) {
                da = new ArrayList();
                Object datos[]=new Object[columnas];
                while(rs.next()){
                    for(int i=0;i<columnas;i++){
                        datos[i]=rs.getObject(i+1);
                    }
                    da.add(datos);
                }
                conect.close();
            }
            return da;
	}catch(SQLException ex){
            JOptionPane.showMessageDialog(null, "Error: "+ex.getMessage());
        }
        return da;
    }

    /**
     * Realiza una busqueda de una columna especifica y la almacena en un JComboBox
     * 
     * @param combo Componete jComboBox que almacenara la informacion
     * @param tabla Nombre de la tabla MySQL
     * @param columna Datos que se almacenaran en el JComboBox
     * @throws SQLException Exepcion en caso de error de conexion.
     */
    public void LlenarJCombo(JComboBox combo,String tabla, String columna) throws SQLException{
        combo.removeAllItems();
        conect = metodospool.dataSource.getConnection();
        st = conect.createStatement();
        try (ResultSet rs = st.executeQuery("SELECT * FROM "+tabla)) {
            while(rs.next()){
                combo.addItem(rs.getString(columna));
            }
            conect.close();
        }
    }
    
    /**
     * Similar a metodo LlenarJCombo(JComboBox, String, String), pero con una busqueda selectiva segun id_bisqueda
     * 
     * @param combo Componete jComboBox que almacenara la informacion
     * @param tabla Nombre de la tabla MySQL
     * @param columna Datos que se almacenaran en el JComboBox
     * @param id Nombre de la columna donde se almacena el valor a filtrar
     * @param id_busqueda Filtra la infomacion y muestra los que poeean referencia a este dato
     * @throws SQLException Exepcion en caso de error de conexion.
     */
    public void LlenarJComboFiltrado(JComboBox combo,String tabla, String columna, String id, String id_busqueda) throws SQLException{
        combo.removeAllItems();
        conect = metodospool.dataSource.getConnection();
        st = conect.createStatement();
        try (ResultSet rs = st.executeQuery("SELECT * FROM "+tabla+" WHERE "+id+"='"+id_busqueda+"'")) {
            while(rs.next()){
                combo.addItem(rs.getString(columna));
            }
            conect.close();
        }
    }
    
    /**
     * Devuelve un dato Texto que coincida con la consulta
     * @param tabla Tabla MySQL
     * @param consulta Columna a consultar
     * @return Retorna el valor de la consulta en formato texto
     * @throws SQLException Exepcion si no se realiza la accion.
     */
    public String LlenarTxt(String tabla, String consulta) throws SQLException{
        String dato = null;
        SSQL = ("SELECT * FROM "+tabla);   
        conect = metodospool.dataSource.getConnection();
        st = conect.createStatement();
        try (ResultSet rs = st.executeQuery(SSQL)) {
            while(rs.next()){
                dato = rs.getString(consulta);
            }
            conect.close();
        }
        return dato;
    }
    
    /**
     *
     * @param tabla Tabla MySQL
     * @param id Nombre que tiene registrada la columna a consultar en MySQL
     * @param idConsulta Valor de la consulta
     * @return Retorna todos los campos de una fila especifica
     * @throws SQLException Execpion en caso de no concretar la consulta
     */
    public String[] SolicitarDataDeUnId(String tabla, String id, String idConsulta) throws SQLException{
        SSQL = ("SELECT * FROM "+tabla+" WHERE "+id+"='"+idConsulta+"';");
        conect = metodospool.dataSource.getConnection();
        st = conect.createStatement();
        String[] datos;
        try (ResultSet rs = st.executeQuery(SSQL)) {
            ResultSetMetaData rsmd = rs.getMetaData();
            datos = new String[rsmd.getColumnCount()];
            while(rs.next()){
                for(int i=0;i<rsmd.getColumnCount();i++){
                    datos[i]=rs.getString(i+1);
                }
            }
            conect.close();
        }
        return datos;
    }
    
    /**
     *
     * @param tabla Tabla MySQL
     * @param id Nombre que recibe la columna en MySQL
     * @param consulta Id a consultar
     * @return Setorna un 1 si es verdadero y un 0 si es falso
     */
    public int ExisteId(String tabla, String id, String consulta){
        int resultado=0;
        SSQL="SELECT * FROM "+tabla+" WHERE "+id+"='"+consulta+"'"; 
        try {
            conect = metodospool.dataSource.getConnection();
            st = conect.createStatement();
            ResultSet rs = st.executeQuery(SSQL);
            if(rs.next()){
                resultado=1;
            }
            conect.close();
        } catch (SQLException ex) {
            System.err.println("Error de conexión"+ex);
        }finally{
            try {
                conect.close();
            } catch (SQLException ex) {
                System.err.println("Error de desconexion"+ex);
            }
        }
    return resultado;
    }
    
    /**
     * Crea una nueva fila con un id unico que almacenara  toda la informacion referente a un objeto
     * 
     * @param tabla Tabla donde se insertaran los datos
     * @param datos Campos que se almacenaran en la tabla en una nueva fila
     * @throws SQLException Exepcion en caso de no completar la accion
     */
    public void CargarDatos(String tabla, String datos) throws SQLException{
        conect = metodospool.dataSource.getConnection();
        st = conect.createStatement();
        st.execute("INSERT into "+tabla+" values('"+datos+"')");
        conect.close();
    }
    
    /**
     * Permite actualizar una Fila de una tabla, siempre y cuando este exista
     * 
     * @param tabla Taba donde se es encuntra el dato a modificar
     * @param campos Campos a modificar de la consulta de Id
     * @param id Nombre del id de la tabla
     * @param idModi Valor del id a consultar y modificar
     * @throws SQLException Exepcion en caso de no completar la accion
     */
    public void Actualizar(String tabla, String campos, String id, String idModi) throws SQLException{
        conect = metodospool.dataSource.getConnection();
        st = conect.createStatement();   
        SSQL="UPDATE "+tabla+" SET "+campos+" WHERE "+id+"='"+idModi+"';";
        st.execute(SSQL);
        conect.close();
    }
    
    /**
     * Elimina una fila completa de una tabla que contenga el mismo ID que el de la consulta
     *
     * @param tabla Tabla MySQL
     * @param idConsulta Consulta especifica
     * @param id Columna de la tabla a borrar.
     * @throws java.sql.SQLException Exepcion en caso de no completar la accion.
     */
    public void EliminarId(String tabla, String idConsulta, String id) throws SQLException{
        conect = metodospool.dataSource.getConnection();
        st = conect.createStatement();
        st.execute("DELETE FROM "+tabla+" WHERE "+id+"='"+idConsulta+"';");
        conect.close();
    }
    
    /**
     * Verifica cual es el ultimo id agregado a una tabla.
     * Metodo solo es valido en columnas numericas.
     * 
     * @param tabla Tabla MySQL
     * @param id iD de la tabla
     * @return El id numerico agregado a la tabla
     * @throws SQLException Exepcion en caso de error de conexion.
     */
    public int UltimoID(String tabla, String id) throws SQLException{
        String temp = null;
        int dato = 0;
        SSQL = ("SELECT MAX("+id+") AS "+id+" FROM "+tabla);
        conect = metodospool.dataSource.getConnection();
        st = conect.createStatement();
        ResultSet rs = st.executeQuery(SSQL);
        while(rs.next()){
            temp = rs.getString(id);
        }
        if(temp!=null){
            dato = Integer.parseInt(temp);
        }
        return dato;
    }
}