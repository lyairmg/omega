/*
 * Copyright (C) 2015 yair
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.porgeeks.cia.herramientas;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import org.jdom2.Document;
import org.jdom2.Element;


/**
 *
 * @author Luis Yair Miranda Gonzalez
 */
public class XML {
    private Document document;
    private File ARCHIVO_XML;
    private String idAtributo, txtAtributo, raiz;
    private Element ele;
    
    public File ruta(String archivo){
        String directorio = System.getProperty("user.dir");// solicito la direccion del programa
        String sep = System.getProperty("file.separator");// aqui obtengo el tipo de separador que hay entre las carpetas
        ARCHIVO_XML = new File(directorio + sep + "src"+sep+"com"+sep+"porgeeks"+sep+"cia"+sep+"informacion"+sep+""+archivo+".xml");
        return ARCHIVO_XML;
    }
    
    public String crearXml(String raiz){
        String xml="";
        String XmlFile = "<"+raiz+">" 
                        + "</"+raiz+">";
        SAXBuilder builder = new SAXBuilder();
        try {
                document = builder.build(new ByteArrayInputStream(XmlFile.getBytes()));
                XMLOutputter outputter1 = new XMLOutputter(Format.getPrettyFormat());
                xml=outputter1.outputString(document);
        } catch (JDOMException | IOException e) {
                System.out.println(e);
        }
        return xml;
    }

    
    public void agregarDatos(String clave, Object valor){
        
        ele.addContent(new Element(clave).setText(valor.toString()));
        
    }
    
    public void atributo(String id, String valor){
        ele = new Element(getRaiz());
        ele.setAttribute(id, valor);
    }
    
    public void agregar(){
        document.getRootElement().addContent(ele);
    }
    
    public void guardar(){
        XMLOutputter outputter1 = new XMLOutputter(Format.getPrettyFormat());
        try {
                FileWriter fw=new FileWriter(ARCHIVO_XML);
                outputter1.output(document, fw);
        } catch (IOException e1) {
            
        }
    }
    
    public Object cargarXml(String busqueda){
    //Se crea un SAXBuilder para poder parsear el archivo
        Object nombre = null;
        SAXBuilder builder = new SAXBuilder();
        try{
            //Se crea el documento a traves del archivo
            document = (Document) builder.build(ARCHIVO_XML);
            //Se obtiene la raiz 'tables'
            Element rootNode = document.getRootElement();

            //Se obtiene la lista de hijos de la raiz 'tables'
            List list = rootNode.getChildren(getRaiz());

            //Se recorre la lista de hijos de 'tables'
            for (Object list1 : list) {
                //Se obtiene el elemento 'tabla'
                Element tabla = (Element) list1;
                //Se obtiene el atributo 'nombre' que esta en el tag 'tabla'
                //            String nombreTabla = tabla.getAttributeValue("nombre");
                //
                //            System.out.println( "Tabla: " + nombreTabla );

                //Se obtiene la lista de hijos del tag 'tabla'
                List lista_campos = tabla.getChildren();
                //Se recorre la lista de campos
                for (Object lista_campo : lista_campos) {
                    //Se obtiene el elemento 'campo'
                    Element campo = (Element) lista_campo;
                    //Se obtienen los valores que estan entre los tags '<campo></campo>'
                    //Se obtiene el valor que esta entre los tags '<nombre></nombre>'
                    nombre = campo.getChildTextTrim(busqueda);
                }
            }
        }catch ( IOException | JDOMException io ) {
            System.out.println( io.getMessage() );
        }
        return nombre;
    }
    

    public String getIdAtributo() {
        return idAtributo;
    }

    public void setIdAtributo(String idAtributo) {
        this.idAtributo = idAtributo;
    }

    public String getTxtAtributo() {
        return txtAtributo;
    }

    public void setTxtAtributo(String txtAtributo) {
        this.txtAtributo = txtAtributo;
    }

    public String getRaiz() {
        return raiz;
    }

    public void setRaiz(String raiz) {
        this.raiz = raiz;
    }
    
    
}
