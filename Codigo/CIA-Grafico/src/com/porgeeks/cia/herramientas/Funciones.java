/*
 * Copyright (C) 2015 YAIE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.porgeeks.cia.herramientas;

import com.porgeeks.cia.sql.ConectarBD;
import java.awt.Image;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.SpinnerDateModel;
import org.apache.commons.codec.digest.DigestUtils;


/**
 * Funciones para optmizar los procesos de el sistema
 * 
 * @author YAIE
 */
public class Funciones {
    ConectarBD conexion;
    Properties propiedades;
    FileOutputStream out;
    private String ruta;
    java.sql.Connection cn = null;
    
    /**
     *
     * @return Retorna la fecha del sistema
     */
    public Date FechaActual(){
        Date fecha = new Date();
        return fecha;
    }

    /**
     * Metodo que solicita ruta de archivo para colocar una imagen en un jLabel no cambia el tamaño
     * 
     * @param path Ruta donde se almacena la imagen a mostrar
     * @param label Laber donde se mostrara la imagen
     */
    public void Imagen(String path, JLabel label){
        URL url = this.getClass().getResource(path);  
        ImageIcon icon = new ImageIcon(url);        
        label.setIcon(new ImageIcon(icon.getImage().getScaledInstance(label.getWidth(), label.getHeight(),Image.SCALE_SMOOTH)));//Agrega la imgagen al laber seleccionado
    }
    
    /**
     *
     * @param spinner Establece valores de tiempo a un Spinner
     */
    public void Hora(JSpinner spinner){
        spinner.setModel(new SpinnerDateModel());
	spinner.setEditor(new JSpinner.DateEditor(spinner, "hh:mm a"));
    }
    
    /**
     * Permine almacenar en milisegundos la hora del sistema
     * 
     * @return Retorna la hora actual del sistema
     */
    public long HoraActual(){
        Date hora = new Date();
        return hora.getTime();
    }
    
    /**
     *
     * @throws SQLException Exepcion en caso de no concretar de forma corecta la conexion
     */
    public void Conection() throws SQLException{     
        try {
            cn=conexion.dataSource.getConnection();
            if(cn!=null){
                System.out.println("Conectado");
            }
        } catch (SQLException e) {
            System.out.println(e);
        }finally{
            try {
                cn.close();
            } catch (SQLException ex) {
                System.err.println(ex);
            }
        }
    }
    
    /**
     * Permite establecer la conexion entre un archivo de prpiedades, para poder manejarlas
     * 
     * @param archivo Ruta del archivo donde se almacenan las propiedades
     * @return Retorna las propiedades del archivo
     */
    public Properties Cargar(String archivo){
        try {
            /**Creamos un Objeto de tipo Properties*/
             propiedades = new Properties(); 
            /**Cargamos el archivo desde la ruta especificada*/
            String directorio = System.getProperty("user.dir");// solicito la direccion del programa
            String sep = System.getProperty("file.separator");// aqui obtengo el tipo de separador que hay entre las carpetas
            ruta = directorio + sep + "src"+sep+"com"+sep+"porgeeks"+sep+"cia"+sep+"informacion"+sep+""+archivo+".properties";
            propiedades.load(new FileInputStream(ruta));            
            return propiedades;
        } catch (FileNotFoundException e) {
            System.err.println("Error, El archivo no exite");
        } catch (IOException e) {
            System.err.println("Error, No se puede leer el archivo");
        }
        return null;
    }
    
    
    public FileOutputStream Archivo(){
        try {
            out = new FileOutputStream(ruta);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Funciones.class.getName()).log(Level.SEVERE, null, ex);
        }
        return out;
    }
    
    /**
     * Metodo simple que llama que usa el recurso DigestUtils para generar una cadena SHA1
     * 
     * @param texto String que sera encriptado
     * @return Retorna el valor de SHA1 del texto encriptado
     */
    public String EncriptarSHA1(String texto){
        String textoEncriptadoConSHA=DigestUtils.sha1Hex(texto); 
        return textoEncriptadoConSHA;
    }
    
}