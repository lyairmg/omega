/*
 * Copyright (C) 2015 YAIE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.porgeeks.cia.graficos.clientes;

import java.text.SimpleDateFormat;
import javax.swing.JOptionPane;
import javax.swing.table.TableColumnModel;
import com.porgeeks.cia.herramientas.Funciones;
import com.porgeeks.cia.sql.FuncionesSQL;
import java.awt.event.KeyEvent;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;
/**
 *
 * @author YAIE
 */
public class NotaVentas extends javax.swing.JFrame {
    Funciones f = new Funciones();
    FuncionesSQL fs = new FuncionesSQL();
    DefaultTableModel modelotabla;
    ArrayList datot=null, productos, equipos;
    boolean terminada=false;
    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
    SimpleDateFormat completa = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
    String fechaConFormato = sdf.format(f.FechaActual());
    String fechacompleta = completa.format(f.FechaActual());
    String campos="idProduc, producNom, producPrecio, 1, 1*producPrecio";
    String[] columnas={"Codigo Art.","Nombre","Precio Unitario","Cantidad","Total"};
    String tabla1 = "empleados";
    String tabla3 = "ventas";
    String id_e="idEmple";
    String nNota="";
    String vendedor;
    
    /**
     * Creates new form NotaVentas
     */
    public NotaVentas() {    
        initComponents();
        this.setTitle("Nueva Venta"); 
        txtFecha.setText(fechaConFormato);
        modelotabla = new DefaultTableModel(null, columnas);
        tCompras.setModel(modelotabla);
        try {
            txtNota.setText(Integer.toString(fs.UltimoID(tabla3, "idNotas")+1));
            nNota = txtNota.getText();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "No es posible conectar al registro de Notas de Venta");
        }
    }
    
    private void Ta(){
        datot.stream().forEach((datot1) -> {
            modelotabla.addRow((Object[]) datot1);
        });
    }

    private void AnchoTabla(){
        TableColumnModel columnModel = tCompras.getColumnModel();
        columnModel.getColumn(0).setPreferredWidth(25);
        columnModel.getColumn(1).setPreferredWidth(250);
        columnModel.getColumn(2).setPreferredWidth(25);
        columnModel.getColumn(3).setPreferredWidth(60);
        columnModel.getColumn(4).setPreferredWidth(60);
    }
    
    private void Completar(){
        try {
            fs.CargarDatos(tabla3, nNota+"','"+fechacompleta+"','"+vendedor);
            for(int i=0;i<tCompras.getRowCount();i++){
                String idcompra= String.valueOf(tCompras.getValueAt(i,0));
                String cantidad = String.valueOf(tCompras.getValueAt(i,3));
                if(fs.ExisteId("equipo", "idEquipo", idcompra)==1){
                    fs.CargarDatos("contiene_equipos", nNota+"','"+idcompra+"','"+cantidad);
                }else if(fs.ExisteId("productos", "idProduc", idcompra)==1){
                    fs.CargarDatos("contiene_productos", nNota+"','"+idcompra+"','"+cantidad);
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(NotaVentas.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void CalcularSub(){
        int totalRow= tCompras.getRowCount();
        totalRow-=1;
        float sumatoria1=0;
        float sumatoria;
        for(int i=0;i<=(totalRow);i++){
            sumatoria= Float.parseFloat(String.valueOf(tCompras.getValueAt(i,4)));
            sumatoria1+=sumatoria;
        }
        float iva=(float) (sumatoria1*0.16);
        txtIVA.setText(String.valueOf(iva));
        txtTotal.setText(String.valueOf(sumatoria1));       
    }
    
    private void Calcular(){
        int fila = tCompras.getSelectedRow();
        if (fila > -1){
            float precio= Float.parseFloat(String.valueOf(tCompras.getValueAt(fila,2)));
            int cantidad = Integer.parseInt(String.valueOf(tCompras.getValueAt(fila,3)));
            float sub = precio*cantidad;
//            txtTotal.setText(String.valueOf(tCompras.getValueAt(fila, 4)));
            tCompras.setValueAt(sub, fila, 4);
            CalcularSub();
        }
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton1 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jButton9 = new javax.swing.JButton();
        jDialog1 = new javax.swing.JDialog();
        jLabel8 = new javax.swing.JLabel();
        jFormattedTextField4 = new javax.swing.JFormattedTextField();
        jLabel1 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        txtFecha = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        bCambiaVendedor = new javax.swing.JButton();
        txtNota = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        txtVendedor = new javax.swing.JFormattedTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tCompras = new javax.swing.JTable();
        jLabel4 = new javax.swing.JLabel();
        bAgregar = new javax.swing.JButton();
        bQuitar = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        txtIVA = new javax.swing.JFormattedTextField();
        txtTotal = new javax.swing.JFormattedTextField();
        jPanel2 = new javax.swing.JPanel();
        bSAlir = new javax.swing.JButton();
        jLabel9 = new javax.swing.JLabel();
        txtEfectivo = new javax.swing.JFormattedTextField();
        bPagar = new javax.swing.JButton();
        txtArticulo = new javax.swing.JFormattedTextField();

        jButton1.setText("jButton1");

        jButton5.setText("jButton5");

        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        jScrollPane2.setViewportView(jTextArea1);

        jButton9.setText("jButton9");

        jLabel8.setText("jLabel8");

        jFormattedTextField4.setText("jFormattedTextField4");

        javax.swing.GroupLayout jDialog1Layout = new javax.swing.GroupLayout(jDialog1.getContentPane());
        jDialog1.getContentPane().setLayout(jDialog1Layout);
        jDialog1Layout.setHorizontalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jFormattedTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(237, Short.MAX_VALUE))
        );
        jDialog1Layout.setVerticalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(jFormattedTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(139, Short.MAX_VALUE))
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);

        jLabel1.setFont(new java.awt.Font("Corbel", 1, 26)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Nota de Venta");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Informacion", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Corbel", 1, 14))); // NOI18N

        txtFecha.setFont(new java.awt.Font("Corbel", 0, 24)); // NOI18N
        txtFecha.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtFecha.setEnabled(false);

        jLabel3.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        jLabel3.setText("Fecha");

        jLabel2.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        jLabel2.setText("Vendedor");

        bCambiaVendedor.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        bCambiaVendedor.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/porgeeks/cia/multimedia/Business-Man-Search-32.png"))); // NOI18N
        bCambiaVendedor.setText("Cambiar");
        bCambiaVendedor.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        bCambiaVendedor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bCambiaVendedorActionPerformed(evt);
            }
        });

        txtNota.setEditable(false);
        txtNota.setBackground(new java.awt.Color(255, 255, 255));
        txtNota.setFont(new java.awt.Font("Corbel", 0, 24)); // NOI18N
        txtNota.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        jLabel10.setText("Nota #");

        try {
            txtVendedor.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("UUU-###")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtVendedor.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtVendedor.setFont(new java.awt.Font("Corbel", 1, 18)); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtVendedor, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(bCambiaVendedor)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtFecha, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel10)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtNota, javax.swing.GroupLayout.DEFAULT_SIZE, 91, Short.MAX_VALUE)
                .addGap(12, 12, 12))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(bCambiaVendedor, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3)
                            .addComponent(jLabel2)
                            .addComponent(jLabel10)
                            .addComponent(txtVendedor, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtNota)
                            .addComponent(txtFecha))))
                .addContainerGap())
        );

        tCompras.setFont(new java.awt.Font("Corbel", 0, 12)); // NOI18N
        tCompras.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Codigo Art.", "Nombre", "Precio Unitario", "Cantidad", "Precio Total"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, true, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tCompras.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        tCompras.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                tComprasFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                tComprasFocusLost(evt);
            }
        });
        tCompras.addMouseWheelListener(new java.awt.event.MouseWheelListener() {
            public void mouseWheelMoved(java.awt.event.MouseWheelEvent evt) {
                tComprasMouseWheelMoved(evt);
            }
        });
        tCompras.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                tComprasMousePressed(evt);
            }
        });
        tCompras.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                tComprasInputMethodTextChanged(evt);
            }
        });
        tCompras.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tComprasKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(tCompras);

        jLabel4.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        jLabel4.setText("Articulo");

        bAgregar.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        bAgregar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/porgeeks/cia/multimedia/Add-To-Cart-32_001.png"))); // NOI18N
        bAgregar.setText("Agregar");
        bAgregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bAgregarActionPerformed(evt);
            }
        });

        bQuitar.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        bQuitar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/porgeeks/cia/multimedia/Cart-Delete-32.png"))); // NOI18N
        bQuitar.setText("Quitar");
        bQuitar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bQuitarActionPerformed(evt);
            }
        });

        jLabel6.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        jLabel6.setText("IVA");

        jLabel7.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        jLabel7.setText("Total");

        txtIVA.setEditable(false);
        txtIVA.setBackground(new java.awt.Color(255, 255, 255));
        txtIVA.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#,##0.00"))));
        txtIVA.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtIVA.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txtIVA.setFont(new java.awt.Font("Corbel", 0, 24)); // NOI18N

        txtTotal.setEditable(false);
        txtTotal.setBackground(new java.awt.Color(255, 255, 255));
        txtTotal.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#,##0.00"))));
        txtTotal.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtTotal.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txtTotal.setFont(new java.awt.Font("Corbel", 0, 24)); // NOI18N

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Completar Compra", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Corbel", 1, 14))); // NOI18N

        bSAlir.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        bSAlir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/porgeeks/cia/multimedia/Cancel-32_001.png"))); // NOI18N
        bSAlir.setText("Cancelar");
        bSAlir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bSAlirActionPerformed(evt);
            }
        });

        jLabel9.setText("Pago:");

        txtEfectivo.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtEfectivo.setFont(new java.awt.Font("Corbel", 0, 24)); // NOI18N

        bPagar.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        bPagar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/porgeeks/cia/multimedia/Payment-02-32.png"))); // NOI18N
        bPagar.setText("Pagar");
        bPagar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bPagarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(jLabel9)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtEfectivo, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(bPagar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(bSAlir)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtEfectivo)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(bSAlir)
                            .addComponent(jLabel9)
                            .addComponent(bPagar))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );

        try {
            txtArticulo.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("UUU-###")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtArticulo.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtArticulo.setFont(new java.awt.Font("Corbel", 0, 24)); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addGap(70, 70, 70)
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtArticulo, javax.swing.GroupLayout.PREFERRED_SIZE, 204, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(bAgregar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bQuitar)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel6, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtIVA, javax.swing.GroupLayout.DEFAULT_SIZE, 98, Short.MAX_VALUE)
                            .addComponent(txtTotal))
                        .addGap(19, 19, 19)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel4)
                        .addComponent(bAgregar)
                        .addComponent(bQuitar))
                    .addComponent(txtArticulo))
                .addGap(17, 17, 17)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6)
                            .addComponent(txtIVA, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel7)))
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void bSAlirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bSAlirActionPerformed
        // TODO add your handling code here:
        int opt = 0;
        if(terminada=true){
            JOptionPane.showConfirmDialog (null, "¿Desea usted cancelar la venta?","Advertencia",opt);
            if(opt == JOptionPane.YES_OPTION){ //The ISSUE is here
                dispose();
            }
        }else{
            dispose();
        }
    }//GEN-LAST:event_bSAlirActionPerformed

    private void bPagarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bPagarActionPerformed
        // Metodo que registra el la venta, el importe pagado y notifica del cambio a entregar
        try{
            float total = Float.parseFloat(txtTotal.getText());
            float pago = Float.parseFloat(txtEfectivo.getText());
            if(pago>total || pago==total){
                Completar();
                JOptionPane.showMessageDialog(null, "Su cambio es: "+(pago-total)+". \n"
                        + "Gracias por realizar su compra", "Compra realizada con Exito",
                        JOptionPane.INFORMATION_MESSAGE);
                terminada=true;
            }else if(pago<total){
                JOptionPane.showMessageDialog(null, "Ingrese una cantidad superior a: "+total+". \n"
                        + "Si no cuneta con fondos suficientes favor de cancelar compra.", "Sin fondos",
                        JOptionPane.ERROR_MESSAGE);
            }
        }catch(NumberFormatException ex){
            JOptionPane.showMessageDialog(null, "Se requiere informacion de pago.");
        }
    }//GEN-LAST:event_bPagarActionPerformed

    private void bCambiaVendedorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bCambiaVendedorActionPerformed
        // TODO add your handling code here:
        vendedor=null;
        try {
            vendedor = txtVendedor.getText();
            if(fs.ExisteId(tabla1, id_e, vendedor)==1){
                JOptionPane.showMessageDialog(null, "El Vendor: "+vendedor+" fue asignado correctamente.");
            }        
        } catch (NullPointerException ex){
            JOptionPane.showMessageDialog(null, "Vendedor "+vendedor+" no fue encontrado");
        }
    }//GEN-LAST:event_bCambiaVendedorActionPerformed

    private void bAgregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bAgregarActionPerformed
        // TODO add your handling code here:
        String busqueda = txtArticulo.getText();
        if(!busqueda.isEmpty()){ 
            if(tCompras.getRowCount()==0){
                datot = fs.AgregarTablaBusqueda("productos", campos, columnas, busqueda, "idProduc", 0);
            }else{
                for(int i=0;i<tCompras.getRowCount();i++){
                    String idcompra= String.valueOf(tCompras.getValueAt(i,0));
                    if(idcompra.equals(busqueda)){
                        JOptionPane.showMessageDialog(null, "Este articulo ya fue ingresa, si desea agregar mas del mismo modifique la cantidad. ");
                        break;
                    }else{
                        datot = fs.AgregarTablaBusqueda("productos", campos, columnas, busqueda, "idProduc", 0);
                        break;
                    }                
                }
            }
        }else{
            JOptionPane.showMessageDialog(null, "Se requiere de un identificador valido");
        }
        Ta();
        Calcular();
        CalcularSub();
        AnchoTabla();
//        fs.RellenarTablaBusqueda(tCompras, tabla1, tabla1, columna, consulta, tabla1);
    }//GEN-LAST:event_bAgregarActionPerformed

    private void tComprasKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tComprasKeyPressed
        // TODO add your handling code here:
        if (evt.getKeyCode() == KeyEvent.VK_ENTER){
            Calcular();
            System.err.println("Press");
        }
    }//GEN-LAST:event_tComprasKeyPressed

    private void tComprasInputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_tComprasInputMethodTextChanged
        // TODO add your handling code here:
        Calcular();
        txtArticulo.requestFocus();
    }//GEN-LAST:event_tComprasInputMethodTextChanged

    private void tComprasMouseWheelMoved(java.awt.event.MouseWheelEvent evt) {//GEN-FIRST:event_tComprasMouseWheelMoved
        // TODO add your handling code here:
        Calcular();
        
    }//GEN-LAST:event_tComprasMouseWheelMoved

    private void tComprasFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tComprasFocusLost
        // TODO add your handling code here:
        Calcular();
    }//GEN-LAST:event_tComprasFocusLost

    private void tComprasMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tComprasMousePressed
        // TODO add your handling code here:
        Calcular();
    }//GEN-LAST:event_tComprasMousePressed

    private void tComprasFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tComprasFocusGained
        // TODO add your handling code here:
        Calcular();
    }//GEN-LAST:event_tComprasFocusGained

    private void bQuitarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bQuitarActionPerformed
        // TODO add your handling code here:
        if(tCompras.getSelectedRow() > -1){
            modelotabla.removeRow(tCompras.getSelectedRow());
        }
        Ta();
    }//GEN-LAST:event_bQuitarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(NotaVentas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new NotaVentas().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bAgregar;
    private javax.swing.JButton bCambiaVendedor;
    private javax.swing.JButton bPagar;
    private javax.swing.JButton bQuitar;
    private javax.swing.JButton bSAlir;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton9;
    private javax.swing.JDialog jDialog1;
    private javax.swing.JFormattedTextField jFormattedTextField4;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JTable tCompras;
    private javax.swing.JFormattedTextField txtArticulo;
    private javax.swing.JFormattedTextField txtEfectivo;
    private javax.swing.JTextField txtFecha;
    private javax.swing.JFormattedTextField txtIVA;
    private javax.swing.JTextField txtNota;
    private javax.swing.JFormattedTextField txtTotal;
    private javax.swing.JFormattedTextField txtVendedor;
    // End of variables declaration//GEN-END:variables
}
