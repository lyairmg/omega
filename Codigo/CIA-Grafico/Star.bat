@echo off
title Cargando Sistema... %time% - %date%
MODE CON COLS=65 LINES=17
set ruta=com\porgeeks\cia\informacion\Servidor.properties
echo.
echo Validando Instalacion...
echo.
echo Verificando Version de Java
java -version
echo Listo.
echo.
echo Buscando configuracion de conexion a MySQL
if exist %ruta% ( goto verificado) else (goto crear)
:crear
jar xf CIA.jar "com\porgeeks\cia\informacion"
:verificado
echo Listo.
echo.
echo Ejecutando...
if exist "CIA.jar" (goto ejecutar) else (goto error)
:error
cls
MODE CON COLS=55 LINES=5
color cf
echo.
echo.
echo       Error! El sistema no se encuentra instalado
echo.
pause > nul
exit
:ejecutar
java -jar CIA.jar
echo Sistema en Linea
:salir
exit