-- MySQL dump 10.13  Distrib 5.6.17, for Win32 (x86)
--
-- Host: 127.0.0.1    Database: ciaomega
-- ------------------------------------------------------
-- Server version	5.6.23-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `clientes`
--


DROP TABLE IF EXISTS `clientes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clientes` (
  `idClientes` char(7) NOT NULL,
  `RFC` char(13) DEFAULT NULL,
  `nomsClie` varchar(50) NOT NULL,
  `apellisClie` varchar(50) DEFAULT NULL,
  `dirClie` varchar(120) DEFAULT NULL,
  `cp` char(5) DEFAULT NULL,
  `sexo` enum('Masculino','Femenino') DEFAULT NULL,
  `fechaIngreso` date DEFAULT NULL,
  PRIMARY KEY (`idClientes`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Datos de Clientes: Nombre, Razon Social Direccion, Genero, Fecha de Ingreso';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clientes`
--

LOCK TABLES `clientes` WRITE;
/*!40000 ALTER TABLE `clientes` DISABLE KEYS */;
INSERT INTO `clientes` VALUES ('CLI-001','0394','Yair','Miranda','Oaxaca','70610','Masculino','2015-06-02'),('CLI-002','0000','Yair','Miranda','Oaxaca SN','70610','Masculino','2015-06-04'),('CLI-003','00000000000','Victoria del Carmen','Garcia Flores','Callejon America','70690','Femenino','2015-06-12');
/*!40000 ALTER TABLE `clientes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `compras`
--

DROP TABLE IF EXISTS `compras`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `compras` (
  `idFactura` varchar(12) NOT NULL,
  `idProvee` char(7) NOT NULL,
  `fechaCompra` date NOT NULL,
  PRIMARY KEY (`idFactura`),
  KEY `idProvee_fk1_idx` (`idProvee`),
  CONSTRAINT `idProvee_fk1` FOREIGN KEY (`idProvee`) REFERENCES `proveedores` (`idProvee`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `compras`
--

LOCK TABLES `compras` WRITE;
/*!40000 ALTER TABLE `compras` DISABLE KEYS */;
/*!40000 ALTER TABLE `compras` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `compras_detalles`
--

DROP TABLE IF EXISTS `compras_detalles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `compras_detalles` (
  `idFactC` char(7) NOT NULL,
  `fechaCompra` date NOT NULL,
  `idProvee` char(7) NOT NULL,
  `idProduc` char(7) NOT NULL,
  `cantidad` smallint(6) NOT NULL,
  `precioProProve` float NOT NULL,
  PRIMARY KEY (`idFactC`,`idProvee`,`idProduc`,`fechaCompra`),
  KEY `idProduc` (`idProduc`),
  KEY `idProvee` (`idProvee`),
  KEY `fechaCompra` (`fechaCompra`),
  CONSTRAINT `compras_detalles_ibfk_2` FOREIGN KEY (`idProduc`) REFERENCES `productos` (`idProduc`) ON UPDATE CASCADE,
  CONSTRAINT `compras_detalles_ibfk_3` FOREIGN KEY (`idProvee`) REFERENCES `proveedores` (`idProvee`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Registro de compras y sus respectivos proveedores, fecha, costos, cantidades adquiridas.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `compras_detalles`
--

LOCK TABLES `compras_detalles` WRITE;
/*!40000 ALTER TABLE `compras_detalles` DISABLE KEYS */;
/*!40000 ALTER TABLE `compras_detalles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contiene`
--

DROP TABLE IF EXISTS `contiene`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contiene` (
  `idNotas` int(11) NOT NULL DEFAULT '1',
  `fecha` datetime NOT NULL,
  `nItem` smallint(3) NOT NULL,
  `idProduc` char(7) DEFAULT NULL,
  `idEquipo` char(7) DEFAULT NULL,
  `cantidad` smallint(6) NOT NULL,
  PRIMARY KEY (`idNotas`,`fecha`,`nItem`),
  KEY `idProduc` (`idProduc`),
  KEY `idEquipo` (`idEquipo`),
  KEY `items` (`nItem`),
  KEY `fecha` (`fecha`),
  CONSTRAINT `contiene_ibfk_2` FOREIGN KEY (`idProduc`) REFERENCES `productos` (`idProduc`) ON UPDATE CASCADE,
  CONSTRAINT `contiene_ibfk_3` FOREIGN KEY (`idEquipo`) REFERENCES `equipo` (`idEquipo`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Detalle de Notas de Venta, resguarda informacion de los productos vendidos en una Venta.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contiene`
--

LOCK TABLES `contiene` WRITE;
/*!40000 ALTER TABLE `contiene` DISABLE KEYS */;
/*!40000 ALTER TABLE `contiene` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `empleados`
--

DROP TABLE IF EXISTS `empleados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `empleados` (
  `idEmple` char(7) NOT NULL,
  `empleNoms` varchar(100) NOT NULL,
  `empleApellis` varchar(100) DEFAULT NULL,
  `empleDir` varchar(150) DEFAULT NULL,
  `cp` char(5) DEFAULT NULL,
  `emailEmple` varchar(100) DEFAULT NULL,
  `sexo` enum('Masculino','Femenino') DEFAULT NULL,
  `edad` date NOT NULL,
  `fechaIngreso` date DEFAULT NULL,
  `idTurno` char(7) NOT NULL,
  `idRango` char(7) NOT NULL,
  `password` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`idEmple`),
  KEY `idRango_idx` (`idRango`),
  KEY `idTurno_idx` (`idTurno`),
  CONSTRAINT `idRango` FOREIGN KEY (`idRango`) REFERENCES `rangos` (`idRango`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `idTurno` FOREIGN KEY (`idTurno`) REFERENCES `turno` (`idTurno`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Datos personales de los empleados y su asignacion de turno y rango';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empleados`
--

LOCK TABLES `empleados` WRITE;
/*!40000 ALTER TABLE `empleados` DISABLE KEYS */;
INSERT INTO `empleados` VALUES ('EMP-001','Luis Yair','Miranda Gonzalez','Oaxaca SN','70610','lyairmg@live.com.mx','Masculino','1994-09-30','2015-06-08','TUR-001','RAN-001','6021e44b0893df4915983209e8e0f95bcb20132a');
/*!40000 ALTER TABLE `empleados` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ensambla`
--

DROP TABLE IF EXISTS `ensambla`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ensambla` (
  `idEquipo` char(7) NOT NULL,
  `idProduc` char(7) NOT NULL,
  `cantidad` smallint(6) NOT NULL,
  PRIMARY KEY (`idProduc`,`idEquipo`),
  KEY `idEquipo` (`idEquipo`),
  CONSTRAINT `ensambla_ibfk_1` FOREIGN KEY (`idEquipo`) REFERENCES `equipo` (`idEquipo`) ON UPDATE CASCADE,
  CONSTRAINT `ensambla_ibfk_2` FOREIGN KEY (`idProduc`) REFERENCES `productos` (`idProduc`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='¡¡¡¡!!!!Tabla de relacion sobre los componetes que posee un equipo ensamblado por la empresa';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ensambla`
--

LOCK TABLES `ensambla` WRITE;
/*!40000 ALTER TABLE `ensambla` DISABLE KEYS */;
/*!40000 ALTER TABLE `ensambla` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `equipo`
--

DROP TABLE IF EXISTS `equipo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `equipo` (
  `idEquipo` char(7) NOT NULL,
  `equiNom` varchar(100) NOT NULL,
  `equiDescri` varchar(200) DEFAULT NULL,
  `equiPrecio` float unsigned NOT NULL,
  PRIMARY KEY (`idEquipo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Descripcion del equipo ensambla y su costo.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `equipo`
--

LOCK TABLES `equipo` WRITE;
/*!40000 ALTER TABLE `equipo` DISABLE KEYS */;
/*!40000 ALTER TABLE `equipo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `productos`
--

DROP TABLE IF EXISTS `productos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productos` (
  `idProduc` char(7) NOT NULL,
  `producNom` varchar(100) NOT NULL,
  `producDescri` mediumtext,
  `producMin` smallint(6) unsigned NOT NULL,
  `producPrecio` float unsigned NOT NULL,
  PRIMARY KEY (`idProduc`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Descripcion de los productos que son suminitrados por un proveedor, un producto puede ser comprado de diferentes proveedores.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `productos`
--

LOCK TABLES `productos` WRITE;
/*!40000 ALTER TABLE `productos` DISABLE KEYS */;
INSERT INTO `productos` VALUES ('COM-001','Asus ModerBoard','DDR3 USB 3.0 INTEL 1770',20,1530),('RAM-001','AData','DDR3 3GB 1600Hz',3,450);
/*!40000 ALTER TABLE `productos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proveedores`
--

DROP TABLE IF EXISTS `proveedores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proveedores` (
  `idProvee` char(7) NOT NULL,
  `proveRFC` char(13) NOT NULL,
  `provNom` varchar(150) NOT NULL,
  `provDir` varchar(150) DEFAULT NULL,
  `provEmail` varchar(100) DEFAULT '@',
  `cp` char(5) DEFAULT '00000',
  `fechaIngreso` date DEFAULT NULL,
  PRIMARY KEY (`idProvee`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Datos personales de los proveedores para contacto o vinculacion de facturas.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proveedores`
--

LOCK TABLES `proveedores` WRITE;
/*!40000 ALTER TABLE `proveedores` DISABLE KEYS */;
INSERT INTO `proveedores` VALUES ('PGS-OLN','00000','Por Geeks','Sin direccion fisccal','admin@porgeeks.com','00000','2015-06-01');
/*!40000 ALTER TABLE `proveedores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rangos`
--

DROP TABLE IF EXISTS `rangos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rangos` (
  `idRango` char(7) NOT NULL,
  `rangDescri` varchar(150) NOT NULL,
  `sueldo` float unsigned NOT NULL,
  `rangNombre` varchar(150) NOT NULL,
  PRIMARY KEY (`idRango`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Niveles de los empleados todos los empleados poseen almenos uno, identifica el novel de acceso y su salario.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rangos`
--

LOCK TABLES `rangos` WRITE;
/*!40000 ALTER TABLE `rangos` DISABLE KEYS */;
INSERT INTO `rangos` VALUES ('RAN-001','Vendedor de mostrador',100,'Vendedor'),('RAN-002','Encargado de finanzas',170,'Contador');
/*!40000 ALTER TABLE `rangos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `requiere`
--

DROP TABLE IF EXISTS `requiere`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `requiere` (
  `idFVentas` char(7) NOT NULL,
  `idNotas` int(11) NOT NULL,
  `idCliente` char(7) NOT NULL,
  `fechaRegistro` date NOT NULL,
  PRIMARY KEY (`idFVentas`,`idNotas`),
  KEY `idCliente_idx` (`idCliente`),
  KEY `idNota_idx` (`idNotas`),
  CONSTRAINT `idCliente_fk` FOREIGN KEY (`idCliente`) REFERENCES `clientes` (`idClientes`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `idNotas_fk` FOREIGN KEY (`idNotas`) REFERENCES `contiene` (`idNotas`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Registro vinculante de las Facturas a un Cliente y unaNota especifica, cada nota solo es posible Facturala de una en una.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `requiere`
--

LOCK TABLES `requiere` WRITE;
/*!40000 ALTER TABLE `requiere` DISABLE KEYS */;
/*!40000 ALTER TABLE `requiere` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `telefonoscliente`
--

DROP TABLE IF EXISTS `telefonoscliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `telefonoscliente` (
  `idClientes` char(7) NOT NULL,
  `telefono` char(15) NOT NULL,
  PRIMARY KEY (`idClientes`,`telefono`),
  CONSTRAINT `telefonoscliente_ibfk_1` FOREIGN KEY (`idClientes`) REFERENCES `clientes` (`idClientes`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `telefonoscliente`
--

LOCK TABLES `telefonoscliente` WRITE;
/*!40000 ALTER TABLE `telefonoscliente` DISABLE KEYS */;
/*!40000 ALTER TABLE `telefonoscliente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `telefonosempleado`
--

DROP TABLE IF EXISTS `telefonosempleado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `telefonosempleado` (
  `idEmple` char(7) NOT NULL,
  `telefono` char(15) NOT NULL,
  PRIMARY KEY (`idEmple`,`telefono`),
  CONSTRAINT `telefonosempleado_ibfk_1` FOREIGN KEY (`idEmple`) REFERENCES `empleados` (`idEmple`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `telefonosempleado`
--

LOCK TABLES `telefonosempleado` WRITE;
/*!40000 ALTER TABLE `telefonosempleado` DISABLE KEYS */;
/*!40000 ALTER TABLE `telefonosempleado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `telefonosproveedor`
--

DROP TABLE IF EXISTS `telefonosproveedor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `telefonosproveedor` (
  `idProvee` char(7) NOT NULL,
  `telefono` char(15) NOT NULL,
  PRIMARY KEY (`idProvee`,`telefono`),
  CONSTRAINT `telefonosproveedor_ibfk_1` FOREIGN KEY (`idProvee`) REFERENCES `proveedores` (`idProvee`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `telefonosproveedor`
--

LOCK TABLES `telefonosproveedor` WRITE;
/*!40000 ALTER TABLE `telefonosproveedor` DISABLE KEYS */;
/*!40000 ALTER TABLE `telefonosproveedor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trabaja`
--

DROP TABLE IF EXISTS `trabaja`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trabaja` (
  `idEmple` char(7) NOT NULL,
  `fecha` date NOT NULL,
  `hRealEnt` time NOT NULL,
  `hRealSal` time NOT NULL,
  PRIMARY KEY (`idEmple`,`fecha`),
  CONSTRAINT `trabaja_ibfk_1` FOREIGN KEY (`idEmple`) REFERENCES `empleados` (`idEmple`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Registro de las entradas y salidas de los empleados';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trabaja`
--

LOCK TABLES `trabaja` WRITE;
/*!40000 ALTER TABLE `trabaja` DISABLE KEYS */;
INSERT INTO `trabaja` VALUES ('EMP-001','2015-06-09','03:51:00','03:55:00');
/*!40000 ALTER TABLE `trabaja` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `turno`
--

DROP TABLE IF EXISTS `turno`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `turno` (
  `idTurno` char(7) NOT NULL,
  `turnDescri` varchar(50) NOT NULL,
  `horaEntra` time NOT NULL,
  `horaSale` time NOT NULL,
  `tolerancia` smallint(3) NOT NULL,
  PRIMARY KEY (`idTurno`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Turnos laborales establecidos por la empresa.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `turno`
--

LOCK TABLES `turno` WRITE;
/*!40000 ALTER TABLE `turno` DISABLE KEYS */;
INSERT INTO `turno` VALUES ('TUR-001','El primer turno del dia','08:00:00','15:00:00',10);
/*!40000 ALTER TABLE `turno` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-06-15 10:59:23
