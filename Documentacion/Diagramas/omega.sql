CREATE DATABASE  IF NOT EXISTS `ciaomega` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `ciaomega`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win32 (x86)
--
-- Host: localhost    Database: ciaomega
-- ------------------------------------------------------
-- Server version	5.6.23-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `clientes`
--

DROP TABLE IF EXISTS `clientes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clientes` (
  `idClientes` char(7) NOT NULL,
  `RFC` char(13) DEFAULT NULL,
  `nomsClie` varchar(50) NOT NULL,
  `apellisClie` varchar(50) DEFAULT NULL,
  `dirClie` varchar(120) DEFAULT NULL,
  `cp` char(5) DEFAULT NULL,
  `sexo` enum('Masculino','Femenino') DEFAULT NULL,
  `fechaIngreso` date DEFAULT NULL,
  PRIMARY KEY (`idClientes`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clientes`
--

LOCK TABLES `clientes` WRITE;
/*!40000 ALTER TABLE `clientes` DISABLE KEYS */;
/*!40000 ALTER TABLE `clientes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contiene`
--

DROP TABLE IF EXISTS `contiene`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contiene` (
  `idNotas` char(7) NOT NULL,
  `idProduc` char(7) NOT NULL,
  `idEquipo` char(7) NOT NULL,
  `cantidad` smallint(6) NOT NULL,
  PRIMARY KEY (`idNotas`,`idProduc`,`idEquipo`),
  KEY `idProduc` (`idProduc`),
  KEY `idEquipo` (`idEquipo`),
  CONSTRAINT `contiene_ibfk_1` FOREIGN KEY (`idNotas`) REFERENCES `notas` (`idNotas`) ON UPDATE CASCADE,
  CONSTRAINT `contiene_ibfk_2` FOREIGN KEY (`idProduc`) REFERENCES `productos` (`idProduc`) ON UPDATE CASCADE,
  CONSTRAINT `contiene_ibfk_3` FOREIGN KEY (`idEquipo`) REFERENCES `equipo` (`idEquipo`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contiene`
--

LOCK TABLES `contiene` WRITE;
/*!40000 ALTER TABLE `contiene` DISABLE KEYS */;
/*!40000 ALTER TABLE `contiene` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `empleados`
--

DROP TABLE IF EXISTS `empleados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `empleados` (
  `idEmple` char(7) NOT NULL,
  `empleNoms` varchar(100) NOT NULL,
  `empleApellis` varchar(100) DEFAULT NULL,
  `empleDir` varchar(150) DEFAULT NULL,
  `cp` char(5) DEFAULT NULL,
  `emailEmple` varchar(100) DEFAULT NULL,
  `fechaIngreso` date DEFAULT NULL,
  `idTurno` char(7) NOT NULL,
  `sexo` enum('Masculino','Femenino') DEFAULT NULL,
  PRIMARY KEY (`idEmple`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empleados`
--

LOCK TABLES `empleados` WRITE;
/*!40000 ALTER TABLE `empleados` DISABLE KEYS */;
/*!40000 ALTER TABLE `empleados` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ensambla`
--

DROP TABLE IF EXISTS `ensambla`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ensambla` (
  `idEquipo` char(7) NOT NULL,
  `idProduc` char(7) NOT NULL,
  `cantidad` smallint(6) NOT NULL,
  PRIMARY KEY (`idProduc`,`idEquipo`),
  KEY `idEquipo` (`idEquipo`),
  CONSTRAINT `ensambla_ibfk_1` FOREIGN KEY (`idEquipo`) REFERENCES `equipo` (`idEquipo`) ON UPDATE CASCADE,
  CONSTRAINT `ensambla_ibfk_2` FOREIGN KEY (`idProduc`) REFERENCES `productos` (`idProduc`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ensambla`
--

LOCK TABLES `ensambla` WRITE;
/*!40000 ALTER TABLE `ensambla` DISABLE KEYS */;
/*!40000 ALTER TABLE `ensambla` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `equipo`
--

DROP TABLE IF EXISTS `equipo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `equipo` (
  `idEquipo` char(7) NOT NULL,
  `equiNom` varchar(100) NOT NULL,
  `equiDescri` varchar(200) DEFAULT NULL,
  `equiPrecio` float unsigned NOT NULL,
  PRIMARY KEY (`idEquipo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `equipo`
--

LOCK TABLES `equipo` WRITE;
/*!40000 ALTER TABLE `equipo` DISABLE KEYS */;
/*!40000 ALTER TABLE `equipo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `facturacompras`
--

DROP TABLE IF EXISTS `facturacompras`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facturacompras` (
  `idFactC` char(7) NOT NULL,
  `fechaExpe` date NOT NULL,
  PRIMARY KEY (`idFactC`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `facturacompras`
--

LOCK TABLES `facturacompras` WRITE;
/*!40000 ALTER TABLE `facturacompras` DISABLE KEYS */;
/*!40000 ALTER TABLE `facturacompras` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `facturaventas`
--

DROP TABLE IF EXISTS `facturaventas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facturaventas` (
  `idFactV` char(7) NOT NULL,
  `fechaExpe` date NOT NULL,
  `idClientes` char(7) DEFAULT NULL,
  PRIMARY KEY (`idFactV`),
  KEY `idClientes` (`idClientes`),
  CONSTRAINT `facturaventas_ibfk_1` FOREIGN KEY (`idClientes`) REFERENCES `clientes` (`idClientes`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `facturaventas`
--

LOCK TABLES `facturaventas` WRITE;
/*!40000 ALTER TABLE `facturaventas` DISABLE KEYS */;
/*!40000 ALTER TABLE `facturaventas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notas`
--

DROP TABLE IF EXISTS `notas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notas` (
  `idNotas` char(7) NOT NULL,
  `fechaExpe` date NOT NULL,
  PRIMARY KEY (`idNotas`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notas`
--

LOCK TABLES `notas` WRITE;
/*!40000 ALTER TABLE `notas` DISABLE KEYS */;
/*!40000 ALTER TABLE `notas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `productos`
--

DROP TABLE IF EXISTS `productos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productos` (
  `idProduc` char(7) NOT NULL,
  `producNom` varchar(100) NOT NULL,
  `producDescri` varchar(200) DEFAULT NULL,
  `producMin` smallint(6) NOT NULL,
  `producPrecio` float unsigned NOT NULL,
  PRIMARY KEY (`idProduc`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `productos`
--

LOCK TABLES `productos` WRITE;
/*!40000 ALTER TABLE `productos` DISABLE KEYS */;
/*!40000 ALTER TABLE `productos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proveedores`
--

DROP TABLE IF EXISTS `proveedores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proveedores` (
  `idProvee` char(7) NOT NULL,
  `provNom` varchar(150) NOT NULL,
  `provDir` varchar(150) DEFAULT NULL,
  `provEmail` varchar(100) DEFAULT NULL,
  `cp` char(5) DEFAULT NULL,
  `fechaIngreso` date DEFAULT NULL,
  PRIMARY KEY (`idProvee`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proveedores`
--

LOCK TABLES `proveedores` WRITE;
/*!40000 ALTER TABLE `proveedores` DISABLE KEYS */;
/*!40000 ALTER TABLE `proveedores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rangos`
--

DROP TABLE IF EXISTS `rangos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rangos` (
  `idRango` char(7) NOT NULL,
  `rangDescri` varchar(150) NOT NULL,
  `sueldo` float NOT NULL,
  PRIMARY KEY (`idRango`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rangos`
--

LOCK TABLES `rangos` WRITE;
/*!40000 ALTER TABLE `rangos` DISABLE KEYS */;
/*!40000 ALTER TABLE `rangos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `requiere`
--

DROP TABLE IF EXISTS `requiere`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `requiere` (
  `idFactV` char(7) NOT NULL,
  `idNotas` char(7) NOT NULL,
  PRIMARY KEY (`idFactV`,`idNotas`),
  CONSTRAINT `requiere_ibfk_1` FOREIGN KEY (`idFactV`) REFERENCES `facturaventas` (`idFactV`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `requiere`
--

LOCK TABLES `requiere` WRITE;
/*!40000 ALTER TABLE `requiere` DISABLE KEYS */;
/*!40000 ALTER TABLE `requiere` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `suministra`
--

DROP TABLE IF EXISTS `suministra`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `suministra` (
  `idFactC` char(7) NOT NULL,
  `idProduc` char(7) NOT NULL,
  `precioProProve` float NOT NULL,
  `cantidad` smallint(6) NOT NULL,
  `idProvee` char(7) NOT NULL,
  PRIMARY KEY (`idFactC`,`idProduc`),
  KEY `idProduc` (`idProduc`),
  KEY `idProvee` (`idProvee`),
  CONSTRAINT `suministra_ibfk_1` FOREIGN KEY (`idFactC`) REFERENCES `facturacompras` (`idFactC`) ON UPDATE CASCADE,
  CONSTRAINT `suministra_ibfk_2` FOREIGN KEY (`idProduc`) REFERENCES `productos` (`idProduc`) ON UPDATE CASCADE,
  CONSTRAINT `suministra_ibfk_3` FOREIGN KEY (`idProvee`) REFERENCES `proveedores` (`idProvee`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `suministra`
--

LOCK TABLES `suministra` WRITE;
/*!40000 ALTER TABLE `suministra` DISABLE KEYS */;
/*!40000 ALTER TABLE `suministra` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `telefonoscliente`
--

DROP TABLE IF EXISTS `telefonoscliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `telefonoscliente` (
  `idClientes` char(7) NOT NULL,
  `telefono` char(15) NOT NULL,
  PRIMARY KEY (`idClientes`,`telefono`),
  CONSTRAINT `telefonoscliente_ibfk_1` FOREIGN KEY (`idClientes`) REFERENCES `clientes` (`idClientes`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `telefonoscliente`
--

LOCK TABLES `telefonoscliente` WRITE;
/*!40000 ALTER TABLE `telefonoscliente` DISABLE KEYS */;
/*!40000 ALTER TABLE `telefonoscliente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `telefonosempleado`
--

DROP TABLE IF EXISTS `telefonosempleado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `telefonosempleado` (
  `idEmple` char(7) NOT NULL,
  `telefono` char(15) NOT NULL,
  PRIMARY KEY (`idEmple`,`telefono`),
  CONSTRAINT `telefonosempleado_ibfk_1` FOREIGN KEY (`idEmple`) REFERENCES `empleados` (`idEmple`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `telefonosempleado`
--

LOCK TABLES `telefonosempleado` WRITE;
/*!40000 ALTER TABLE `telefonosempleado` DISABLE KEYS */;
/*!40000 ALTER TABLE `telefonosempleado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `telefonosproveedor`
--

DROP TABLE IF EXISTS `telefonosproveedor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `telefonosproveedor` (
  `idProvee` char(7) NOT NULL,
  `telefono` char(15) NOT NULL,
  PRIMARY KEY (`idProvee`,`telefono`),
  CONSTRAINT `telefonosproveedor_ibfk_1` FOREIGN KEY (`idProvee`) REFERENCES `proveedores` (`idProvee`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `telefonosproveedor`
--

LOCK TABLES `telefonosproveedor` WRITE;
/*!40000 ALTER TABLE `telefonosproveedor` DISABLE KEYS */;
/*!40000 ALTER TABLE `telefonosproveedor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trabaja`
--

DROP TABLE IF EXISTS `trabaja`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trabaja` (
  `idEmple` char(7) NOT NULL,
  `fecha` date NOT NULL,
  `hRealEnt` time NOT NULL,
  `hRealSal` time NOT NULL,
  PRIMARY KEY (`idEmple`,`fecha`),
  CONSTRAINT `trabaja_ibfk_1` FOREIGN KEY (`idEmple`) REFERENCES `empleados` (`idEmple`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trabaja`
--

LOCK TABLES `trabaja` WRITE;
/*!40000 ALTER TABLE `trabaja` DISABLE KEYS */;
/*!40000 ALTER TABLE `trabaja` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `turno`
--

DROP TABLE IF EXISTS `turno`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `turno` (
  `idTurno` char(7) NOT NULL,
  `turnDescri` varchar(50) NOT NULL,
  `horaEntra` time NOT NULL,
  `horaSale` time NOT NULL,
  PRIMARY KEY (`idTurno`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `turno`
--

LOCK TABLES `turno` WRITE;
/*!40000 ALTER TABLE `turno` DISABLE KEYS */;
/*!40000 ALTER TABLE `turno` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-03-15 20:06:00
